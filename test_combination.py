import time
import random
a = time.time()

from itertools import combinations

def work():
    tab1 = [random.randint(1,1000) for i in range(100)]
    tab2 = [random.randint(1,1000) for i in range(100)]

    print(tab1, tab2)

    merge = []

    for i in range(1,6):
        comb1 = combinations(tab1, i)
        for j in range(1, 6):
            comb2 = combinations(tab2,j)
            merge.append([[(x, y) for x in comb1 if sum(x) == sum(y)] for y in comb2])

    my_list = []
    for t in merge:
        for o in t:
            if(o):
                print(o)
                my_list.append(o)

    b = time.time()

    print(b-a)
    return my_list

work()