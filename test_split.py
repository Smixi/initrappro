import random
import datetime
from datetime import date, time, timedelta
import pandas as pd
import itertools

class Transaction():
    def __init__(self, amount: int=None, date:date=None, reference: int=None, bank_type:str = None, is_tax:bool = False, transaction_number: int = -1, is_collection_transaction=False):
        self.amount: int = amount
        self.date: date = date
        self.reference: int = reference
        self.type: int = bank_type
        self.is_tax: bool = is_tax
        self.transaction_number: int = transaction_number
        self.is_collection_transaction: bool = is_collection_transaction
        self.piece_type: str = "SK" if self.is_collection_transaction else "ZR"
    def __str__(self):
        return "amount: {0}, date: {1}, reference(corelation): {2}, bank_ref: {3}, is_tax:{4}, transaction_number:{5}, isCollectionTransaction(ZR/ZK): {6}".format(self.amount, self.date,
         self.reference, self.type, self.is_tax, self.transaction_number, self.is_collection_transaction)
    def __repr__(self):
        return self.__str__()

class PaymentConfig():
    def __init__(self, transaction_min_amount:int = 0, transaction_max_amount: int=10000, max_split_number: int = 1,
    max_day_delay_in: int = 5, max_day_delay_out: int = 5, min_delay_in: int = 0, min_delay_out: int = 0, account_type: str = None, max_split_number_in: int = 1,
    has_taxes: bool = False, diff: list = [0,0], is_tax_included: bool = False,
    min_split_number_in: int = 1, min_split_number: int = 1):
        self.transaction_min_amount = transaction_min_amount
        self.transaction_max_amount = transaction_max_amount
        self.max_split_number = max_split_number
        self.max_day_delay_in = max_day_delay_in
        self.max_day_delay_out = max_day_delay_out
        self.min_delay_in = min_delay_in
        self.min_delay_out = min_delay_out
        self.account_type = account_type
        self.max_split_number_in = max_split_number_in
        self.has_taxes = has_taxes
        self.diff = diff
        self.is_tax_included = is_tax_included
        self.min_split_number_in = min_split_number_in
        self.min_split_number = min_split_number

def generate_data(number_of_transactions: int = 5,transaction_min_amount:int=0,
 transaction_max_amount: int=1000, max_split_number: int=3, max_day_delay_in: int=5, max_day_delay_out=5, min_delay_in: int=2, min_delay_out:int = 2, from_config_dict:dict=None):
    in_transaction = []
    out_transaction = []

    if(from_config_dict == None):
        raise NotImplementedError("A refaire de la même manière qu'avec le dict")
        bank_types = {"RECH":"RCH", "CESP":"ESP", "RECB":"ECB","REAM":"EAM", "RCUP":"ECB"}
        bank_types_list = list(bank_types)
        #On créer autant de transaction que demandé
        for transaction_number in range(number_of_transactions):
            transaction_bank_type = random.choice(bank_types_list)
            transaction_amount = random.randrange(transaction_min_amount, transaction_max_amount)
            transaction_date = date.today() + timedelta(days=random.randint(min_delay_in,max_day_delay_in))
            to_split_amount_in = transaction_amount
            in_transaction.append(Transaction(transaction_amount, transaction_date, transaction_number, transaction_bank_type, is_collection_transaction=True))
            #On va splitté cette transaction aléatoirement.
            to_split_amount = transaction_amount
            #On créer des transactions n-1 fois en soustrayant un montant aléatoire.
            for split_number in range(random.randint(0, max_split_number-1)):
                new_transaction_amount = random.randrange(to_split_amount)
                to_split_amount = to_split_amount - new_transaction_amount
                #Ajoute une transaction qui a lieu max_delay_out plus tard
                out_transaction.append(Transaction(new_transaction_amount, transaction_date + timedelta(days=random.randint(min_delay_out,max_day_delay_out)),
                transaction_number, bank_types[transaction_bank_type], is_collection_transaction=True))
            #La dernière transaction
            out_transaction.append(Transaction(to_split_amount, transaction_date + timedelta(days=random.randint(min_delay_out,max_day_delay_out)), transaction_number, bank_types[transaction_bank_type]))
        return in_transaction, out_transaction
    else:
        #On obtient les infos via le dictionnaire de config
        bank_types_list = list(from_config_dict)
        #On créer autant de transaction que demandé
        t_number = 0
        reverse_t_number = 0
        for transaction_number in range(number_of_transactions):
            transaction_bank_type = random.choice(bank_types_list)
            transaction_amount = random.randrange(from_config_dict[transaction_bank_type].transaction_min_amount, from_config_dict[transaction_bank_type].transaction_max_amount)
            transaction_date = date.today() + timedelta(days=random.randint(from_config_dict[transaction_bank_type].min_delay_in, from_config_dict[transaction_bank_type].max_day_delay_in))
            to_split_amount_in = round(transaction_amount + random.uniform(from_config_dict[transaction_bank_type].diff[0], from_config_dict[transaction_bank_type].diff[1]), 2)
            
            #On subdivise la transaction avec le nombre de fois autorisé
            splits_in = from_config_dict[transaction_bank_type].max_split_number_in-1
            splits_in_min = max(0,from_config_dict[transaction_bank_type].min_split_number_in-1)
            tax_sum = 0
            for split_number_in in range(random.randint(splits_in_min, splits_in)):
                t_number = t_number + 1
                new_transaction_amount_in = round(random.uniform(0,to_split_amount_in),2)
                to_split_amount_in = to_split_amount_in - new_transaction_amount_in
                #Transaction négative
                in_transaction.append(Transaction(new_transaction_amount_in,
                transaction_date, transaction_number, transaction_bank_type, False, t_number, is_collection_transaction=True))
                #Ajout en cas de taxes
                
                if(from_config_dict[transaction_bank_type].has_taxes):
                    tax = round(random.uniform(0.01, 0.03)*new_transaction_amount_in,2)
                    tax_sum += tax
                    in_transaction.append(Transaction(-tax, transaction_date, transaction_number, transaction_bank_type, True, t_number, is_collection_transaction=True))
            
            #Dernière transaction
            t_number = t_number + 1
            in_transaction.append(Transaction( round(to_split_amount_in,2),
            transaction_date, transaction_number, transaction_bank_type, False, t_number, is_collection_transaction=True))
            
            #Ajout en cas de taxes
            if(from_config_dict[transaction_bank_type].has_taxes):
                tax = round(random.uniform(0.01, 0.03)*to_split_amount_in,2)
                tax_sum += tax
                in_transaction.append(Transaction(-tax, transaction_date, transaction_number, transaction_bank_type, True, t_number, is_collection_transaction=True))
                
            #On va splitté cette transaction aléatoirement pour les relevés bancaires.
            to_split_amount = transaction_amount - tax_sum
            #On créer des transactions n-1 fois en soustrayant un montant aléatoire.
            
            for split_number in range(random.randint(max(0,from_config_dict[transaction_bank_type].min_split_number-1), from_config_dict[transaction_bank_type].max_split_number-1)):
                reverse_t_number -= 1
                new_transaction_amount = random.randrange(to_split_amount)
                to_split_amount = to_split_amount - new_transaction_amount
                #Ajoute une transaction qui a lieu max_delay_out plus tard
                out_transaction.append(Transaction(new_transaction_amount, transaction_date + timedelta(days=random.randint(from_config_dict[transaction_bank_type].min_delay_out, from_config_dict[transaction_bank_type].max_day_delay_out)),
                transaction_number, from_config_dict[transaction_bank_type].account_type, transaction_number=reverse_t_number))
            #La dernière transaction
            reverse_t_number -= 1
            out_transaction.append(Transaction(to_split_amount, transaction_date + timedelta(days=random.randint(from_config_dict[transaction_bank_type].min_delay_out, from_config_dict[transaction_bank_type].max_day_delay_out)),
            transaction_number, from_config_dict[transaction_bank_type].account_type, transaction_number=reverse_t_number))
        return in_transaction, out_transaction



# print("----- Fichier encaissement ----- ")
# for t in in_t:
#     print(t)
# print("----- Fichier à rapproché avec réponse ----- ")
# for t in out_t:
#     print(t)

#bank_types = {"RECH":"RCH", "CESP":"ESP", "RECB":"ECB","REAM":"EAM", "RCUP":"ECB"}

pcRCH = PaymentConfig(1,50000,2,0,11,0,5,"RCH", 1, False, [0, 0])
pcCESP = PaymentConfig(1,50000,2,0,11,0,7,"ESP", 1, False, [0, 0])
pcRECB = PaymentConfig(1,50000,2,0,5,0,1,"ECB", 2, False, [0, 0])
pcREAM = PaymentConfig(1,50000,1,0,11,0,7,"EAM", 2, True, [0.01, 0.99], is_tax_included=True)
pcRCUP = PaymentConfig(1,50000,1,0,5,0,1,"ECB", 1, False, [0, 0])

in_t, out_t = generate_data(number_of_transactions=50, from_config_dict={"RECH": pcRCH, "CESP":pcCESP,"RECB":pcRECB, "REAM":pcREAM,"RCUP":pcRCUP})

print("----- Fichier encaissement ----- ")
for t in in_t:
    print(t)
print("----- Fichier à rapproché avec réponse ----- ")
for t in out_t:
    print(t)


columns_dict = {"Référence": "type", "Icône Postes rappr./PNS": None, "Nº pièce": "transaction_number", "Domaine d'activité": None, "Type de pièce":"piece_type", "Date pièce": "date", "Date comptable": "date", "Clé comptabilisation": None, "Montant en devise interne": "amount", 
"Devise interne": None, "Code TVA": None, "Montant devise document": "amount", "Devise pièce": None, "Pièce rapprochement": None, "Texte": None, "Clé de référence 3":"type", "Compte": None, "Date valeur":"date", "Lettrage":"reference"}

#Dataframe pour ecriture excel

headers = list(columns_dict.keys())
df = pd.DataFrame([[t.__dict__[columns_dict[k]] if columns_dict[k] is not None else "Vide" for k in columns_dict.keys() ] for t in itertools.chain(in_t, out_t)])

df.to_excel("output.xlsx", header=headers, index=False)